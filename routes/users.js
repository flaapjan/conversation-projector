var express = require('express');
var router = express.Router();


/* GET users listing. */
router.get('/', function(req, res, next) {
});

let USERS = {
  'flaap': 'Test1234'
};

router.put('/', function(req, res, next) {
  console.info('Request with data >>>> ', req.body, req.body.data);
  console.info(' user data = ', USERS[req.body.data.username]);

  try {

    console.info('Request with data >>>> ', req.body, req.body.data.username, req.body.data.password);
    let instance = USERS[req.body.data.username];

    if(instance && instance === req.body.data.password) {
      res.status(200).send();
      res.end('request successful');
    } else {
      res.status(404).send({});
      res.end('request successful : no content');
    }
  }
   catch(e) {
    console.info(' ERROR :: ', USERS[req.body.data.username], req.body, e);
  }
});

module.exports = router;
